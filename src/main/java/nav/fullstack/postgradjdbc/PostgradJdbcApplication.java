package nav.fullstack.postgradjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class PostgradJdbcApplication  {
    public static void main(String[] args) {
        SpringApplication.run(PostgradJdbcApplication.class, args);
    }
}
