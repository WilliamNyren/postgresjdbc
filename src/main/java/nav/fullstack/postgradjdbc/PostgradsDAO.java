package nav.fullstack.postgradjdbc;



import nav.fullstack.postgradjdbc.Models.Student;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;


//TODO: NB, denne klassen skal ikke  i oppgaven, bruk kun Repositories, de skal håndtere data access
//TODO: Dette er ikke en todo
@Component
public class PostgradsDAO {
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;

    public void test() {
        System.out.println("Attempt to connect to postgrads database");
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            System.out.println("Connection success");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Student> getAllStudents() {
        String sql = "SELECT * FROM student";
        ArrayList<Student> students = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();

            while(result.next()) {
                Student student = new Student(
                        result.getInt("stud_id"),
                        result.getString("stud_name"),
                        result.getInt("supervisor_id")
                );

                students.add(student);
            }
            System.out.println(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }

    public void getStudentById(int id) {
        String sql = "SELECT * FROM student WHERE stud_id = ?";

        try(Connection conn = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);

            ResultSet result = statement.executeQuery();

            while(result.next()) {
                System.out.println(
                        new Student(
                                result.getInt(1),
                                result.getString(2),
                                result.getInt(3)
                        )
                );

            } //printer ut en record av student

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertStudent(Student student) {
        String sql = "INSERT INTO student VALUES (?,?,?)";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, student.id());
            statement.setString(2, student.name());
            statement.setInt(3, student.supervisor());

            int result = statement.executeUpdate();
            System.out.println(result);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
