package nav.fullstack.postgradjdbc.Models;

public record Student(
        int id,
        String name,
        int supervisor
) {
}
