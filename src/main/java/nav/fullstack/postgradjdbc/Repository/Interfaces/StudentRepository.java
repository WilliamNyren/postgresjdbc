package nav.fullstack.postgradjdbc.Repository.Interfaces;

import nav.fullstack.postgradjdbc.Models.Student;

public interface StudentRepository extends CRUDRepository<Student, Integer> {
    public void test();
}
