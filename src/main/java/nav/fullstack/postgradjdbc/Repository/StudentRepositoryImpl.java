package nav.fullstack.postgradjdbc.Repository;

import nav.fullstack.postgradjdbc.Models.Student;
import nav.fullstack.postgradjdbc.Repository.Interfaces.StudentRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepositoryImpl implements StudentRepository {
    private final String url;
    private final String username;
    private final String password;

    public StudentRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password
    ) {
        this.url = url;
        this.username = username;
        this.password = password;
    }


    public void test() {
        System.out.println("Attempt to connect to postgrads database");
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            System.out.println("Connection success");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Student> findAll() {
        String sql = "SELECT * FROM student";
        ArrayList<Student> students = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();

            while(result.next()) {
                Student student = new Student(
                        result.getInt("stud_id"),
                        result.getString("stud_name"),
                        result.getInt("supervisor_id")
                );

                students.add(student);
            }
            System.out.println(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }

    @Override
    public Student findById(Integer id) {
        String sql = "SELECT * FROM student WHERE stud_id = ?";
        Student student = null;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);

            ResultSet result = statement.executeQuery();
            result.next();


            student = new Student(
                    result.getInt(1),
                    result.getString(2),
                    result.getInt(3)
            );

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return student;
    }

    @Override
    public int insert(Student student) {
        String sql = "INSERT INTO student VALUES (?,?,?)";
        int result = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, student.id());
            statement.setString(2, student.name());
            statement.setInt(3, student.supervisor());

            result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int update(Student student) {
        String sql = "UPDATE student SET stud_name = ?, supervisor_id = ? WHERE stud_id = ?";
        int result = 0;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, student.name());
            statement.setInt(2, student.supervisor());
            statement.setInt(3, student.id());
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public int delete(Student student) {
        return deleteById(student.id());
    }

    @Override
    public int deleteById(Integer id) {
        String sql = "DELETE FROM student WHERE stud_id = ?";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;

    }

}
