package nav.fullstack.postgradjdbc.Runner;

import nav.fullstack.postgradjdbc.Models.Student;
import nav.fullstack.postgradjdbc.Repository.Interfaces.StudentRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.sql.SQLOutput;

@Component
public class JDBCAppRunner implements ApplicationRunner {
    private final StudentRepository studentRepo;


    public JDBCAppRunner(StudentRepository studentRepo) {
        this.studentRepo = studentRepo;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Spring boot kjorer!!");

        studentRepo.test();

        System.out.println(studentRepo.findAll());

        System.out.println(studentRepo.findById(3));

        Student student = new Student(9, "Carl Såne", 1);
        System.out.println(studentRepo.insert(student));

        Student updateStudent = new Student(9, "Carl Såne", 2);
        System.out.println(studentRepo.update(updateStudent));

        System.out.println(studentRepo.delete(student));
    }
}
